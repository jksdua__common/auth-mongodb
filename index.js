'use strict';

var debug = require('debug')('@knoxxnxt/auth-mongodb');

/**
 * Initialize mongodb session middleware with `opts`:
 *
 * @param {Object} options
 *   - {MongoCollection} [collection]                 monk collection
 *   - {Mixed}           [uri=localhost/test]         uri passed to monk
 *   - {Object}          [opt]                        options passed to monk
 *   - {String}          [collectionName=users]       name of the collection to use
 */
var MongodbStore = module.exports = function (options) {
  if (!(this instanceof MongodbStore)) {
    return new MongodbStore(options);
  }

  options = this.options = options || {};

  var collection = options.collection;
  if (!collection) {
    options.uri = options.uri || 'localhost/test';
    options.collectionName = options.collectionName || 'users';

    // only load monk module if needed
    var db = options.db = require('monk')(options.uri, options.opt);

    collection = options.collection = db.get(options.collectionName);
  }

  this.options = options;
  this.collection = collection;

  debug('creating instance with options: %j', options);
  debug('instance uses collection: %s', collection.name);

  // index email
  this.collection.ensureIndex({ email: 1 }, { unique: true, sparse: true });
};

// Generates a random database id
MongodbStore.prototype.id = function() {
  debug('id');
  return this.collection.id();
};

// An arguments hash passed from application level code is passed straight to the function. This could be used for various querying options.
MongodbStore.prototype.find = function() {
  debug('find: %j', arguments);
  return this.collection.find.apply(this.collection, arguments);
};

// The user id is passed. It expects the entire user model to be returned.
MongodbStore.prototype.findById = function(id) {
  debug('findById: %j', arguments);
  return this.collection.findById(id);
};

// Fetch a user by email. It expects the entire user model to be returned.
MongodbStore.prototype.findByEmail = function(email) {
  debug('findByEmail: %j', arguments);
  return this.collection.findOne({ email: email });
};

/*
  A user json object is sent. The user object contains nested properties that must be handled appropriately by the data store. It expects a boolean indicating if the user was successfully inserted into the database.

  It is left to the data store to assign the user with an id. This must be set as _id field on the user object
 */
MongodbStore.prototype.insert = function(user) {
  debug('insert: %j', arguments);
  delete user.pass;
  return this.collection.insert(user);
};

// An array of user json objects is sent. It expects a boolean indicating if the user was successfully inserted into the database.
MongodbStore.prototype.bulkInsert = function(users) {
  debug('bulkInsert: %j', arguments);
  return this.collection.insert(users);
};

// The entire user object is passed to the data store. It expects a boolean indicating if the user was successfully inserted into the database.
MongodbStore.prototype.update = function(user) {
  debug('update: %j', arguments);
  return this.collection.updateById(user._id, user);
};

// Remove a user from the database using their email address.
MongodbStore.prototype.removeByEmail = function(email) {
  debug('removeByEmail: %j', arguments);
  return this.collection.remove({ email: email });
};

// Remove all the database records
MongodbStore.prototype.reset = function() {
  debug('reset: %j', arguments);
  return this.collection.remove({});
};