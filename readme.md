@knoxxnt/auth-mongodb
=========

Mongodb datastore for `@knoxxnxt/auth` versions ~6.1.x

## Usage

`@knoxxnxt/auth-mongodb` works with [@knoxxnxt/auth](https://gitlab.com/jksdua__common/auth) (an authentication library).

```bash
$ npm i @knoxxnxt/auth-mongodb
```

## Example

```javascript
var auth = require('js_auth')({
  db: require('js_auth-mongodb')()
});
```

## Options

Pass either a monk collection or databse uri, opt and collection name.

```
 *   {MongoCollection} [collection]                 monk collection
 *   {Mixed}           [uri=localhost/test]         uri passed to monk
 *   {Object}          [opt]                        options passed to monk
 *   {String}          [collectionName=users]       name of the collection to use
```

## Changelog

### v6.1.0-0.0.1 (7 Dec '15)

- Version bumped

### v6.0.0-0.0.1 (23 Jul '15)

- Changed name to **@knoxxnxt/auth-mongodb**
- Updated to work with `@knoxxnxt/auth` 6+

### v5.0.0-0.0.1 (18 Feb '14)

- Support for `js_auth` 5.0.x
- **Tests are failing**

### v4.0.0-1.0.0 (11 Feb '14)

- Initial commit
