'use strict';

var spawn = require('child_process').spawn;
var datastore = require('./store');

function runTests() {
  return function(done) {
    var test = spawn('npm', ['run', 'actual-test']);

    test.stdout.on('data', function(d) {
      console.info(d + '');
    });
    test.stderr.on('data', function(d) {
      console.info(d + '');
    });
    test.on('close', function(code) {
      done(null, code);
    });
  };
}

require('co')(function *() {
  yield datastore.reset();
  return yield runTests();
}).then(process.exit);